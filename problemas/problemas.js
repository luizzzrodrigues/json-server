const uuid = require('uuid/v1');
const casual = require('casual');

const randomTrue = () => Math.random() > 0.5;

const diagnostico = () => ({
  id: uuid(),
  descricao: 'Dengue Não Muito Grave',
});

const zeroOrOne = () => casual.coin_flip ? 1 : 0;

const getOrigem = () => casual.coin_flip ? 'S' : 'A';

const getObservacao = () => ({
  idUsuario: uuid(),
  idProblema: uuid(),
  data: casual.date(),
  comentario: casual.text
})

const getObservacoes = quantity => {
  const response = [];
  const limit = quantity === 0 ? 0 : quantity ? quantity : 1;
  for(var i = 0; i < limit; i++) {
    response.push(getObservacao())
  }

  return response;
}

// Id Usuário
// ID Problema
// Data
// Comentário

module.exports = {
  data: [
    {
      id: uuid(),
      idPaciente: uuid(),
      idEmpresa: uuid(),
      primecare: casual.coin_flip,
      dataInicio: casual.date(),
      dataInicioAproximada: null,
      dataFim: null,
      dataFinalAproximada: null,
      origem: getOrigem(),
      diagnostico: diagnostico(),
      cronico: casual.coin_flip,
      status: zeroOrOne(),
      idProfissionalInformante: uuid(),
      dataInformado: casual.date(),
      observacoes: getObservacoes(0)
    },
    {
      id: uuid(),
      idPaciente: uuid(),
      idEmpresa: uuid(),
      primecare: randomTrue(),
      dataInicio: casual.date(),
      dataInicioAproximada: null,
      dataFim: null,
      dataFinalAproximada: null,
      origem: getOrigem(),
      diagnostico: diagnostico(),
      cronico: randomTrue(),
      status: zeroOrOne(),
      idProfissionalInformante: uuid(),
      dataInformado: casual.date(),
      observacoes: getObservacoes(3)
    },
    {
      id: uuid(),
      idPaciente: uuid(),
      idEmpresa: uuid(),
      primecare: randomTrue(),
      dataInicio: casual.date(),
      dataInicioAproximada: null,
      dataFim: null,
      dataFinalAproximada: null,
      origem: getOrigem(),
      diagnostico: diagnostico(),
      cronico: randomTrue(),
      status: zeroOrOne(),
      idProfissionalInformante: uuid(),
      dataInformado: casual.date(),
      observacoes: getObservacoes(1)
    },
    {
      id: uuid(),
      idPaciente: uuid(),
      idEmpresa: uuid(),
      primecare: randomTrue(),
      dataInicio: casual.date(),
      dataInicioAproximada: null,
      dataFim: null,
      dataFinalAproximada: null,
      origem: getOrigem(),
      diagnostico: diagnostico(),
      cronico: randomTrue(),
      status: zeroOrOne(),
      idProfissionalInformante: uuid(),
      dataInformado: casual.date(),
      observacoes: getObservacoes(1)
    },
    {
      id: uuid(),
      idPaciente: uuid(),
      idEmpresa: uuid(),
      primecare: randomTrue(),
      dataInicio: casual.date(),
      dataInicioAproximada: null,
      dataFim: null,
      dataFinalAproximada: null,
      origem: getOrigem(),
      diagnostico: diagnostico(),
      cronico: randomTrue(),
      status: zeroOrOne(),
      idProfissionalInformante: uuid(),
      dataInformado: casual.date(),
      observacoes: getObservacoes(1)
    },
  ],
};
