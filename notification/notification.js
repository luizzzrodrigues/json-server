const uuid = require('uuid/v1');
const casual = require('casual');

module.exports = [
  {
    _id: uuid(),
    message: 'Atualização de sistema xpto',
    messageData: {
      section: 'Agenda',
      description: casual.text,
    },
    metaData: {
      jiraKey: 'CW-1234',
      environment: 'cw_prod',
      trigger: 'prontuario.inicio',
    },
  },
  {
    _id: uuid(),
    message: 'Atualização de sistema xpto',
    messageData: {
      section: 'Agenda',
      description: casual.text,
    },
    metaData: {
      jiraKey: 'CW-1234',
      environment: 'cw_prod',
      trigger: 'prontuario.inicio',
    },
  },
  {
    _id: uuid(),
    message: 'Atualização de sistema xpto',
    messageData: {
      section: 'Cadastro',
      description: casual.text,
    },
    metaData: {
      jiraKey: 'CW-1234',
      environment: 'cw_prod',
      trigger: 'prontuario.inicio',
    },
  },
  {
    _id: uuid(),
    message: 'Atualização de sistema xpto',
    messageData: {
      section: 'Financeiro',
      description: casual.sentence,
    },
    metaData: {
      jiraKey: 'CW-1234',
      environment: 'cw_prod',
      trigger: 'prontuario.inicio',
    },
  },
  {
    _id: uuid(),
    message: 'Atualização de sistema xpto',
    messageData: {
      section: 'Relatorios',
      description: casual.sentence,
    },
    metaData: {
      jiraKey: 'CW-1234',
      environment: 'cw_prod',
      trigger: 'prontuario.inicio',
    },
  },
];
