module.exports = function() {
  return {
    notification: require('./notification/notification'),
    interactions: [],
    problemas: require('./problemas/problemas')
  };
};
