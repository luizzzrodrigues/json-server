module.exports = (req, res, next) => {
  if ('query' in req) {
    req.query = {};
  }

  next();
};
